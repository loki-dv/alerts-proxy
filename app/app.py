from flask import Flask, request
import requests
import os

app = Flask(__name__)

# Will get it from envs
# TODO provide checking of vars
token = os.getenv('API_TOKEN')
chat_id = os.getenv('CHAT_ID')

# Settings
url = f'https://api.telegram.org/{token}/sendMessage'
parse_mode = 'html'


@app.route('/send', methods=['POST'])
def proxy():
    data = request.values
    # Check data
    # TODO provide more checking, add try-catch block
    if 'host' not in data:
        return "You need to introduce yourself!\n", 444
    else:
        hostname = data['host']
    if 'message' not in data:
        return "You need to tell me something!\n", 444
    else:
        message = data['message']
    if 'result' not in data:
        return 'You need to provide some result!\n', 444
    else:
        if data['result'] in ['fail', 'success', 'ok', 'error', 'err']:
            if data['result'] in ['success', 'ok']:
                text = f"&#9989; <b>INFO</b>\n<pre>{hostname}: {message}</pre>"
            else:
                text = f"&#9940; <b>ERROR</b>\n<pre>{hostname}: {message}</pre>"
    params = {'text': text, 'parse_mode': parse_mode, 'chat_id': chat_id}
    r = requests.post(url, data=params)
    if r.status_code == 200:
        result_message = "Success\n"
        result_code = 200
    else:
        result_message = "Something went wrong"
        result_code = 444
    return result_message, result_code


if __name__ == "__main__":
    app.run(host='0.0.0.0')
