# Alerts proxy

## Description

It is simple flask application provides API interface for sending alerts via telegram bot.

## DEV mode

You can run this service locally

```
cd app
FLASK_APP=app.py flask run
```

Also you must provide env vars `API_TOKEN` and `CHAT_ID`

For testing you can try to send a message:

```
curl -X POST http://localhost:5000/send -d "host=test" -d "message=Backup finished successfully" -d "result=success" 
```



TODO: 
* Add SSL certificate for provide full safe connection
* Check TODOs in the app like additional checks etc
