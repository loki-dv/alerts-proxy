FROM python:3.8-alpine

COPY app /app

RUN pip install -r /app/requirements.txt && pip cache purge

LABEL version="1.0"
LABEL maintainer="Nikita Babich <loki.dv@gmail.com>"

EXPOSE 5000

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "--chdir", "/app", "--log-level", "debug", "wsgi:app"]
